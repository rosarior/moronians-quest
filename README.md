Attack of the Moronians - Quest edition
=======================================

In the year 2023 an alien civilization tries to enslave humanity by stealing
all books and knowledge. Only a brave hero with the required skills can
save humanity from a moronic future.

License
-------
This project is open sourced under [Apache License 2.0](http://www.apache.org/licenses/).


Author
------
Roberto Rosario - [Twitter](http://twitter.com/#siloraptor) [E-mail](mailto://roberto.rosario.gonzalez@gmail.com)

How to play
-----------
./game.py


WASD keys to move
Mouse to point and shoot
Escape to exit the game
Pause/Break key to pause the game


Screenshots
-----------
![Title screen](https://raw.gitlab.com/rosarior/moronians/master/docs/_static_/title_screen.png)
![Story](https://raw.gitlab.com/rosarior/moronians/master/docs/_static_/story_1.png)
![Story](https://raw.gitlab.com/rosarior/moronians/master/docs/_static_/story_2.png)
![Game play](https://raw.gitlab.com/rosarior/moronians/master/docs/_static_/stage.png)
![Boss battle](https://raw.gitlab.com/rosarior/moronians/master/docs/_static_/boss.png)
