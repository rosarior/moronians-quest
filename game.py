#!/usr/bin/env python

import argparse

from libraries.engine import Game


def main():
    module_name = None
    fullscreen = False

    parser = argparse.ArgumentParser(description='Launch the game engine.')
    parser.add_argument(
        '-m', '--module', action='store', dest='module',
        help='dotted path to the game module class to use',
        type=str
    )
    parser.add_argument(
        '-f', '--fullscreen', action='store_true', dest='fullscreen'
    )

    args = parser.parse_args()

    game = Game(
        fullscreen=args.fullscreen,
        game_module_dotted_path=args.module
    )
    game.run()


if __name__ == '__main__':
   main()
