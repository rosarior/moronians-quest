import pygame
from pygame.math import Vector2

from ..literals import (
    COLOR_ALMOST_BLACK, COLOR_WHITE, INTERVAL_INVINCIBLE, TEAM_BAD_BULLET,
    TEAM_GOOD, TEAM_GOOD_BULLET
)
from ..utils import outlined_text

from .base import Actor


class ActorBullet(Actor):
    attack_points = 10
    #animation_normal_files = ['players/W_Book06.png']
    #animation_normal_dimensions = [(34, 34)]
    animation_normal_files = ['bullets/yellow_spinning.png']
    animation_normal_dimensions = [(16, 14)]
    fps = 14
    sound_spawn_file = 'assets/bullets/shot.ogg'
    speed = 1
    animation_death = None
    team = TEAM_GOOD_BULLET
    team_allies = (TEAM_BAD_BULLET, TEAM_GOOD)

    def __init__(self, *args, **kwargs):
        #self.font = kwargs.pop('font', None)
        self.init_position = kwargs.pop('init_position', None)
        self.speed = kwargs.pop('speed', None) or self.speed
        #self.direction = kwargs.pop('direction')

        super().__init__(*args, **kwargs)

    def on_calculate_movement(self):
        #self.set_direction(vec2d(direction_x, direction_y).normalized())

        self.pos = self.pos + self.direction * self.speed

        bounds_rect = self.game.surface.get_rect()

        if self.pos.x < bounds_rect.left:
            self.on_death()
        elif self.pos.x + self.size[0] > bounds_rect.right:
            self.on_death()

        if self.pos.y < bounds_rect.top:
            self.on_death()
        elif self.pos.y + self.size[1] > bounds_rect.bottom:
            self.on_death()

    def on_hit(self, foreign_actor):
        self.on_death()

    def on_death(self):
        super().on_death()
        self.stage.actor_remove(actor=self)
        self.destroy()

    def on_setup(self, stage):
        super().on_setup(stage=stage)

        if self.init_position:
            self.pos = Vector2(self.init_position)


class ActorPlayer(Actor):
    animation_normal_files = [
        'players/boy_walk_down_stripe.png', 'players/boy_walk_up_stripe.png',
        'players/boy_walk_right_stripe.png'
    ]
    animation_normal_dimensions = [(34, 34), (34, 34), (34, 34)]
    animation_death_file = 'players/boy_death_stripe.png'
    animation_death_dimensions = (34, 34)
    attack_points = 10
    answer = []
    can_overflow = False
    fps = 8
    hit_score_penalty = 100
    mouse_angle = Vector2(0, 0)
    score = 0
    sound_hit_file = 'assets/players/04.ogg'
    sound_die_file = 'assets/players/falldown.wav'
    speed = 0.08
    team = TEAM_GOOD
    team_allies = (TEAM_GOOD_BULLET,)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.music_win = 'assets/players/141695__copyc4t__levelup.wav'
        #self.scroll = pygame.image.load('assets/players/I_Scroll02.png').convert_alpha()
        #self.scroll_speed = 0.008
        #self.thought_image = pygame.image.load('assets/players/thought.png').convert_alpha()
        self.last_shot = pygame.time.get_ticks()
        self.gun_cooldown = 450
        self.image_cross_hair = ActorPlayer.load_sliced_sprites(
            width=35, height=35, filename='players/crosshairs/crosshair6.png', origin_y=0
        )[0]
        self.image_pistol = ActorPlayer.load_sliced_sprites(
            width=34, height=23, filename='guns/pistol.png', origin_y=0
        )[0]

    def on_blit(self):

        location_gun = Vector2(0, 0)
        location_gun.from_polar((25, self.mouse_angle))

        angle_gun = 360 - self.mouse_angle

        image = self.image_pistol
        if angle_gun > 180 and angle_gun < 260 or angle_gun > 450:
            image = pygame.transform.flip(self.image_pistol, False, True)
            angle_gun += 5
        else:
            angle_gun -= 5

        image = pygame.transform.rotate(image, angle_gun)

        final_gun_location = location_gun + self.get_position_center(other_image=image)
        if final_gun_location.y < self.pos.y:
            self.game.surface.blit(image, final_gun_location)
            super().on_blit()
        else:
            super().on_blit()
            self.game.surface.blit(image, final_gun_location)


        cross_hair_location = Vector2(0, 0)
        cross_hair_location.from_polar((80, self.mouse_angle))

        image = self.image_cross_hair
        self.game.surface.blit(image, cross_hair_location + self.get_position_center(other_image=image))


    def on_calculate_movement(self):
        super().on_calculate_movement()
        if self.game.running and self.is_alive():
            keys_pressed = pygame.key.get_pressed()

            direction_y = 0
            direction_x = 0

            # Do not use elif to allow diagonal movement
            if keys_pressed[pygame.K_a]:
                direction_x = -1
            if keys_pressed[pygame.K_d]:
                direction_x = 1
            if keys_pressed[pygame.K_w]:
                direction_y = -1
            if keys_pressed[pygame.K_s]:
                direction_y = 1

            self.set_direction(Vector2(direction_x, direction_y))

    def on_death(self):
        super().on_death()
        self.answer = []
        self.stage.on_game_over()
        self._time_death = pygame.time.get_ticks()
        self.accept_input = False
        self.set_animation_fps(15)

    def on_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            self.do_fire()

        """
        elif event.type == pygame.KEYDOWN and not self.game.paused:
            #if event.key == pygame.K_q:
            #    self.do_fire()

            if event.key not in [pygame.K_a, pygame.K_d, pygame.K_w, pygame.K_s]:
                if event.key == pygame.K_RETURN and self.answer:
                    self.game.get_current_stage().player_shot(self, ''.join(self.answer))
                    self.answer = []
                elif event.key == pygame.K_BACKSPACE:
                    self.answer = self.answer[0: -1]
                elif event.key <= 127 and event.key >= 32:
                    self.answer.append(chr(event.key))
        """
    def do_fire(self):
        ticks = pygame.time.get_ticks()
        if ticks > self.last_shot + self.gun_cooldown:
            self.last_shot = ticks

            bullet_location = Vector2(0, 0)
            bullet_location.from_polar((40, self.mouse_angle))

            bullet_location += self.get_position_center(other_size=(16, 14))

            bullet = ActorBullet(
                game=self.game, init_position=bullet_location,
                speed=.2,
            )

            self.stage.actor_add(actor=bullet)
            bullet.set_direction(
                Vector2(pygame.mouse.get_pos() - self.pos).normalize()
            )
            bullet.show()

    def on_hit(self, foreign_actor):
        super().on_hit(foreign_actor=foreign_actor)
        if self.is_alive() and not self.invincible:
            displacement = Vector2(
                foreign_actor.direction.x * 50,
                foreign_actor.direction.y * 50
            )

            self.pos += displacement
            self.rect.topleft = [self.pos.x, self.pos.y]

            self.set_invincible(INTERVAL_INVINCIBLE)
            self.score -= self.hit_score_penalty
            if self.score < 0:
                self.score = 0

    def on_setup(self, stage):
        super().on_setup(stage)
        self.pos.x = self.game.surface.get_size()[0] / 2 - self.size[0] / 2
        self.pos.y = self.game.surface.get_size()[1] / 2 - self.size[1] / 2
        self._sound_die_played = False
        self.result_font = self.game.font
        self.has_scroll = False
        # left, up, right, down
        # 0   , 1 , 2    , 3
        self.direction_facing = 0
        self.direction_facing_old = 0

    def on_update(self, time_passed, force=False):
        super(ActorPlayer, self).on_update(time_passed=time_passed)

        if not self.game.running:
            self._invincible_initial_time += time_passed

        if self.is_alive():
            self.mouse_angle = Vector2(
                pygame.mouse.get_pos() - self.pos
            ).as_polar()[1]

            if self.mouse_angle >= -45 and self.mouse_angle <= 45:
                # Right
                self.direction_facing = 2
            elif self.mouse_angle >= 45 and self.mouse_angle <= 135:
                # Down
                self.direction_facing = 3
            elif self.mouse_angle >= 135 or self.mouse_angle <= -135:
                # Left
                self.direction_facing = 0
            elif self.mouse_angle >= -135 and self.mouse_angle <= -45:
                # Up
                self.direction_facing = 1

            if self.direction_facing_old != self.direction_facing:
                if self.direction_facing == 2:
                    # Right
                    self.set_flip_x(False)
                    self.change_animation(self.animation_normal[2])
                elif self.direction_facing == 3:
                    # Down
                    self.change_animation(self.animation_normal[0])
                elif self.direction_facing == 0:
                    # Left
                    self.set_flip_x(True)
                    self.change_animation(self.animation_normal[2])
                elif self.direction_facing == 1:
                    # Up
                    self.change_animation(self.animation_normal[1])

            self.direction_facing_old = self.direction_facing

            if self.direction != Vector2(0, 0):
                self.animation_active = True
            else:
                self.animation_active = False
                self._frame = 0

        if not self.is_alive():
            if pygame.time.get_ticks() > self._time_death + 1400:
                self.image = pygame.transform.rotozoom(self.animation_normal[0][0], 90, 1)
                if not self._sound_die_played:
                    self.sound_die.play()
                    self._sound_die_played = True

        if self.invincible:
            if pygame.time.get_ticks() > self._invincible_initial_time:
                self.invincible = False

        if self.invincible:
            self.strobe_start()
        else:
            self.strobe_stop()

    def on_win_scroll(self):
        if not self.has_scroll:
            self.has_scroll = True
            pygame.mixer.music.load(self.music_win)
            pygame.mixer.music.play()
            #self.scroll_direction = (vec2d(self.pos[0], 0) - vec2d(self.pos)).normalized()
            #self.scroll_position = ((self.pos[0] + self.size[0] / 2) - self.scroll.get_size()[0] / 2, self.pos[1] - 80)
            #self.scroll_original_position = self.scroll_position
            self._time_win_time = pygame.time.get_ticks()

    def reset_scroll(self):
        self.has_scroll = False


Actor.register(klass=ActorBullet)
Actor.register(klass=ActorPlayer)
