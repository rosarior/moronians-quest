from .base import Actor


class ActorHumanShip(Actor):
    animation_normal_files = ['players/human_ship.png']
    animation_normal_dimensions = [(60, 48)]
    fps = 45


class ActorSpaceship(Actor):
    animation_normal_files = ['enemies/bio_ship.png']
    animation_normal_dimensions = [(48, 62)]


class ActorTracktorBeam(Actor):
    animation_normal_files = ['enemies/tractor_beam.png']
    animation_normal_dimensions = [(12, 100)]
