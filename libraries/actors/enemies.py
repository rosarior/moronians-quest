from random import random, randint

import pygame
from pygame.math import Vector2

from ..literals import TEAM_BAD, TEAM_BAD_BULLET, TEAM_GOOD, TEAM_GOOD_BULLET

from .base import Actor
from .player import ActorBullet, ActorPlayer


class ActorBossBullet(ActorBullet):
    animation_normal_files = ['bullets/bad/sprite_strip.png']
    animation_normal_dimensions = [(19, 15)]
    sound_spawn_file = 'assets/bullets/bad/fire.ogg'
    sound_spawn_volume = 0.5
    team = TEAM_BAD_BULLET
    team_allies = (TEAM_BAD, TEAM_GOOD_BULLET)


class ActorEnemyBossFiringPatternSniperMixin:
    animation_shoot = None
    sniper_firing_delay = 1000
    sniper_firing_speed = .12
    sniper_firing_class = ActorBossBullet
    sniper_firing_delay_jitter = 3000
    _last_sniper_firing = 0

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        team_allies = set(self.team_allies)
        team_allies.add(TEAM_BAD_BULLET)
        self.team_allies = tuple(team_allies)

    def on_sniper_fire(self):
        #self.speed = 0
        if self.animation_shoot:
            self.change_animation(self.animation_shoot)
            self.set_animation_fps(10)

        bullet_location = Vector2(0, 0)
        bullet_location.from_polar((10, 90))

        bullet_location += self.get_position_center(
            other_size=self.sniper_firing_class.animation_normal_dimensions[0]
        )

        bullet = self.sniper_firing_class(
            game=self.game, init_position=bullet_location,
            speed=self.sniper_firing_speed
        )

        self.stage.actor_add(bullet)
        bullet.set_direction(
            Vector2(
                Vector2(
                    self.game.player.get_position_center()
                ) - Vector2(self.get_position_center())
            ).normalize()
        )
        bullet.show()

        self._last_sniper_firing = pygame.time.get_ticks()

    def on_update(self, time_passed, force=False):
        super().on_update(time_passed=time_passed)

        if self.is_alive():
            if pygame.time.get_ticks() > self._last_sniper_firing + self.sniper_firing_delay + random() * self.sniper_firing_delay_jitter:
                self.on_sniper_fire()


class ActorEnemy(Actor):
    animation_death_file = 'enemies/smoke_puff_strip.png'
    animation_death_dimensions = (32, 32)
    attack_points = 1
    can_overflow = False
    total_hit_points = 1
    sound_die_file = 'assets/sounds/8bit_bomb_explosion.wav'
    team = TEAM_BAD
    team_allies = (TEAM_BAD_BULLET,)

    def __init__(self, *args, **kwargs):
        self.font = kwargs.pop('font', None)
        self.init_position = kwargs.pop('init_position', None)
        self.speed = kwargs.pop('speed', None) or self.speed

        super().__init__(*args, **kwargs)

    def on_animation_finished(self):
        # Remove dead actor after their death animation finishes
        if not self.is_alive():
            if self in self.stage.enemies:
                self.stage.enemies.remove(self)
                self.destroy()

    def on_blit(self):
        super().on_blit()

    def on_calculate_movement(self):
        # Follow player
        self.set_direction((self.game.player.pos - self.pos).normalize())

    def on_death(self):
        super().on_death()
        self.game.player.score += self.score_value

    def on_setup(self, stage):
        super().on_setup(stage)

        if self.init_position:
            self.pos = Vector2(self.init_position)


class ActorEnemyEyePod(
    ActorEnemyBossFiringPatternSniperMixin,
    ActorEnemy
):
    animation_normal_files = ['enemies/eye_pod_strip.png']
    animation_normal_dimensions = [(32, 32)]
    attack_points = 5
    fps = 8
    score_value = 100
    speed = 0.005


class ActorEnemyRedSlime(ActorEnemy):
    animation_normal_files = ['enemies/redslime_strip.png']
    animation_normal_dimensions = [(32, 32)]
    attack_points = 10
    fps = 10
    score_value = 150
    speed = 0.01


class ActorEnemyArachnid(ActorEnemy):
    animation_normal_files = ['enemies/aracnid_strip.png']
    animation_normal_dimensions = [(32, 32)]
    attack_points = 15
    fps = 12
    score_value = 200
    speed = 0.025


class ActorEnemyFlyingBot(ActorEnemy):
    animation_normal_files = ['enemies/flying_bot_strip.png']
    animation_normal_dimensions = [(32, 32)]
    attack_points = 20
    fps = 14
    score_value = 300
    speed = 0.05


class ActorEnemyBossMinionSpawnMixin:
    minion_spawn_delay = 2000
    minion_speed = ActorEnemyEyePod.speed * 8
    minion_class = ActorEnemyEyePod
    _last_minion_spawn = 0

    def on_update(self, time_passed, force=False):
        super().on_update(time_passed=time_passed)

        if self.is_alive():
            if pygame.time.get_ticks() > self._last_minion_spawn + self.minion_spawn_delay:
                self.on_minion_spawn()

    def on_minion_spawn(self):
        self.speed = 0
        self.change_animation(self.animation_shoot)
        self.set_animation_fps(5)

        self.stage.spawn_enemy(
            self.minion_class,
            origin_point=(self.pos[0] + self.image.get_size()[0] / 2,
            self.pos[1] + self.image.get_size()[1]),
            speed=self.minion_speed
        )
        self._last_minion_spawn = pygame.time.get_ticks()


class ActorEnemyBoss(
    ActorEnemyBossFiringPatternSniperMixin,
    ActorEnemyBossMinionSpawnMixin,
    ActorEnemy
):
    animation_death_fps = 0.01
    fps = 1
    score_value = 1000
    sound_die_file = 'assets/enemies/zombie-17.wav'
    sound_hit_file = 'assets/enemies/zombie-5.wav'
    team_allies = (TEAM_GOOD,)

    def __init__(self, *args, **kwargs):
        super(ActorEnemyBoss, self).__init__(*args, **kwargs)
        self._move_time = 0
        self.original_speed = self.speed

    def on_calculate_movement(self):
        bounds_rect = self.game.surface.get_rect()

        if self.rect.left <= bounds_rect.left:
            self.direction.x *= -1
        elif self.rect.right >= bounds_rect.right:
            self.direction.x *= -1
        elif self.rect.top <= bounds_rect.top:
            self.direction.y *= -1
        elif self.rect.bottom >= bounds_rect.bottom:
            self.direction.y *= -1

    def on_animation_finished(self):
        if self.is_alive():
            self.speed = self.original_speed
            self.change_animation(self.animation_normal[0])
            self.set_animation_fps(self.fps)
            self._move_time = pygame.time.get_ticks()

    def on_death(self):
        super().on_death()
        self.stage.on_level_complete()

    def on_hit(self, foreign_actor):
        super().on_hit(foreign_actor=foreign_actor)
        self.speed = 0
        self.change_animation(self.animation_hit)
        self.set_animation_fps(6)
        self._last_shot = pygame.time.get_ticks()

    def on_setup(self, stage):
        super().on_setup(stage)

        self.init_position = (randint(0, self.stage.screen_size[0]), 0)
        self.pos = Vector2(self.init_position)
        self.animation_shoot = Actor.load_sliced_sprites(
            *self.animation_shoot_dimensions, filename=self.animation_shoot_file
        )
        self.animation_hit = Actor.load_sliced_sprites(
            *self.animation_hit_dimensions, filename=self.animation_hit_file
        )
        self.direction = self.default_direction


class ActorEnemyBossHorizontal(ActorEnemyBoss):
    animation_hit_file = 'enemies/dark_boss_hit.png'
    animation_hit_dimensions = (122, 110)
    animation_normal_files = ['enemies/dark_boss_walk.png']
    animation_normal_dimensions = [(122, 110)]
    animation_shoot_file = 'enemies/dark_boss_shoot.png'
    animation_shoot_dimensions = (122, 110)

    attack_points = 20

    default_direction = Vector2(1, 0)  # Horizontal

    enemy_class = ActorEnemyEyePod

    firing_animation_delay = 150
    firing_delay = 1100

    sound_spawn_file = 'assets/enemies/alert.ogg'

    speed = 0.1
    total_hit_points = 150
    score_value = 5000


class ActorEnemyBossVertical(ActorEnemyBoss):
    animation_hit_file = 'enemies/boss_genie_hit.png'
    animation_hit_dimensions = (80, 96)
    animation_normal_files = ['enemies/boss_genie_walk.png']
    animation_normal_dimensions = [(80, 96)]
    animation_shoot_file = 'enemies/boss_genie_shoot.png'
    animation_shoot_dimensions = (80, 96)

    attack_points = 20

    default_direction = Vector2(0, 1)  # Down

    enemy_class = ActorEnemyRedSlime

    firing_animation_delay = 150
    firing_delay = 1000
    speed = 0.1
    total_hit_points = 150
    score_value = 10000


class ActorEnemyBossBounce(ActorEnemyBoss):
    animation_hit_file = 'enemies/boss_phantom_hit.png'
    animation_hit_dimensions = (100, 100)
    animation_normal_files = ['enemies/boss_phantom_walk.png']
    animation_normal_dimensions = [(100, 100)]
    animation_shoot_file = 'enemies/boss_phantom_shoot.png'
    animation_shoot_dimensions = (100, 100)

    attack_points = 20

    default_direction = Vector2(1, 1)  # Down

    enemy_class = ActorEnemyArachnid
    firing_animation_delay = 150
    firing_delay = 900
    score_value = 20000

    sound_spawn_file = 'assets/enemies/alert.ogg'

    speed = 0.1
    total_hit_points = 150


Actor.register(klass=ActorBossBullet)
Actor.register(klass=ActorEnemyBossBounce)
Actor.register(klass=ActorEnemyBossHorizontal)
