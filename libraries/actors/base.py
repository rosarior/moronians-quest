import itertools
import os

import pygame

from pygame.math import Vector2


class Actor(object):
    _registry = []

    animation_death_file = None
    animation_death_dimensions = (0, 0)
    animation_death_fps = 8
    animation_loop = True
    animation_active = True
    animation_normal_files = []
    animation_normal_dimensions = [(0, 0)]
    attack_points = 1
    total_hit_points = 100
    invincible = False
    fps = 1
    sound_hit = None
    sound_hit_file = None
    sound_die = None
    sound_die_file = None
    speed = 0.05
    team = None

    sound_spawn = None
    sound_spawn_file = None
    sound_spawn_volume = 0.0

    speed = 0
    scale = 1
    rotation = 0
    flip_x = False
    flip_y = False
    can_overflow = True
    team_allies = None

    @staticmethod
    def load_sliced_sprites(width, height, filename, origin_y=0):
        images = []
        master_image = pygame.image.load(os.path.join('assets', filename))  # .convert_alpha()

        master_width, master_height = master_image.get_size()
        for i in range(int(master_width / width)):
            images.append(master_image.subsurface((i * width, origin_y, width, height)))
        return images

    @classmethod
    def register(cls, klass):
        cls._registry.append(klass)

    @classmethod
    def setup_class(cls):
        if cls.sound_spawn_file:
            cls.sound_spawn = pygame.mixer.Sound(cls.sound_spawn_file)
            if cls.sound_spawn_volume:
                cls.sound_spawn.set_volume(cls.sound_spawn_volume)

    @classmethod
    def setup_classes(cls):
        for klass in cls._registry:
            klass.setup_class()

    def __init__(self, game):
        self.game = game
        self._visible = False
        self._strobe = False
        self.team_allies = self.team_allies or (self.team,)

    def change_animation(self, images):
        self.images = images
        self._frame = -1
        self._last_update = 0
        self.rect = self.images[0].get_rect()
        self.size = self.images[0].get_size()
        self.image = self.images[0]
        self.mask = pygame.mask.from_surface(self.image)

    def destroy(self):
        if self in self.stage.actors:
            self.stage.actors.remove(self)

    def get_position_center(self, other_image=None, other_size=None):
        position_x = self.pos[0] + self.image.get_size()[0] / 2
        position_y = self.pos[1] + self.image.get_size()[1] / 2

        if other_image:
            position_x -= other_image.get_size()[0] / 2
            position_y -= other_image.get_size()[1] / 2
        elif other_size:
            position_x -= other_size[0] / 2
            position_y -= other_size[1] / 2

        return (position_x, position_y)

    def hide(self):
        self._visible = False
        self._strobe = False

    def is_alive(self):
        return self.hit_points > 0

    def on_animate(self, time_passed, force=False):
        if self.animation_active or force:
            t = pygame.time.get_ticks()
            if t - self._last_update > self._animation_delay or force:
                self._frame += 1
                if self._frame >= len(self.images):
                    if self.animation_loop:
                        self._frame = 0
                    else:
                        self._frame -= 1

                    self.on_animation_finished()

                self.image = self.images[self._frame]
                self.mask = pygame.mask.from_surface(self.image)
                self._last_update = t

    def on_animation_finished(self):
        pass

    def on_blit(self):
        if self._visible:
            temp = pygame.transform.rotozoom(self.image, self.rotation, self.scale)
            if self.flip_x or self.flip_y:
                temp = pygame.transform.flip(temp, self.flip_x, self.flip_y)
            self.game.surface.blit(temp, self.pos)

    def on_calculate_movement(self):
        if self.destination:
            if int(self.pos.x) != int(self.destination.x) or int(self.pos.y) != int(self.destination.y):
                self.set_destination(self.destination.x, self.destination.y, self.speed)
            else:
                self.speed = 0

    def on_collision(self, foreign_actor):
        if foreign_actor.team not in self.team_allies:
            self.on_hit(foreign_actor=foreign_actor)
        else:
            self.on_touch(foreign_actor=foreign_actor)

    def on_death(self):
        self.hit_points = 0
        self.direction = Vector2(0, 0)

        if self.sound_die:
            self.sound_die.play()

        self.animation_loop = False
        if self.animation_death_fps:
            self.set_animation_fps(self.animation_death_fps)

        if self.animation_death:
            self.change_animation(self.animation_death)

    def on_execute(self):
        pass

    def on_event(self, event):
        pass

    def on_hit(self, foreign_actor):
        self.hit_points -= foreign_actor.attack_points
        if self.sound_hit:
            self.sound_hit.play()

        if self.hit_points <= 0:
            self.hit_points = 0
            self.on_death()

    def on_setup(self, stage):
        self.stage = stage

        self.direction = Vector2(0, 0)
        self.pos = Vector2(0, 0)
        self.destination = Vector2(0, 0)

        if self.sound_die_file:
            self.sound_die = pygame.mixer.Sound(self.sound_die_file)
        if self.sound_hit_file:
            self.sound_hit = pygame.mixer.Sound(self.sound_hit_file)

        if self.animation_death_file:
            self.animation_death = Actor.load_sliced_sprites(*self.animation_death_dimensions, filename=self.animation_death_file)

        self.animation_normal = []

        for filename, dimensions in zip(self.animation_normal_files, self.animation_normal_dimensions):
            self.animation_normal.append(Actor.load_sliced_sprites(*dimensions, filename=filename))

        self._invincible_initial_time = 0
        self.change_animation(self.animation_normal[0])
        self.hit_points = self.total_hit_points
        self.set_animation_fps(self.fps)
        self._cache_game_surface = None

    def on_touch(self, foreign_actor):
        pass

    def get_game_surface_rect(self):
        if not self._cache_game_surface:
            self._cache_game_surface = self.game.surface.get_rect()
        return self._cache_game_surface

    def on_update(self, time_passed, force=False):
        if self._strobe:
            self._visible = not self._visible

        if self.is_alive():
            self.on_calculate_movement()

        displacement = Vector2(
            self.direction.x * self.speed * time_passed,
            self.direction.y * self.speed * time_passed
        )

        self.pos += displacement

        # TODO: cache this value
        bounds_rect = self.get_game_surface_rect()#self.game.surface.get_rect()

        if not self.can_overflow:
            if self.pos.x < bounds_rect.left:
                self.pos.x = bounds_rect.left
            elif self.pos.x + self.size[0] > bounds_rect.right:
                self.pos.x = bounds_rect.right - self.size[0]

            if self.pos.y < bounds_rect.top:
                self.pos.y = bounds_rect.top
            elif self.pos.y + self.size[1] > bounds_rect.bottom:
                self.pos.y = bounds_rect.bottom - self.size[1]

        self.rect.topleft = [self.pos.x, self.pos.y]

        for actor in itertools.combinations(self.stage.actors, 2):
            # Text has no rect attribute
            if not actor[0] == actor[1] and hasattr(actor[0], 'rect') and hasattr(actor[1], 'rect'):
                if pygame.sprite.collide_mask(actor[0], actor[1]) and actor[0].is_alive() and actor[1].is_alive():
                    if actor[0].team != actor[1].team:
                        if not actor[0].invincible:
                            actor[0].on_collision(actor[1])

                        if not actor[1].invincible:
                            actor[1].on_collision(actor[0])

        """
        for local_actor in self.stage.actors:
            for foreign_actor in self.stage.actors:
                if not local_actor == foreign_actor and hasattr(foreign_actor, 'rect') and hasattr(local_actor, 'rect'):
                    if pygame.sprite.collide_mask(local_actor, foreign_actor) and local_actor.is_alive() and foreign_actor.is_alive():
                        if local_actor.team != foreign_actor.team:
                            if not local_actor.invincible:
                                local_actor.on_collision(foreign_actor)

                            if not foreign_actor.invincible:
                                foreign_actor.on_collision(local_actor)
        """
        self.on_animate(time_passed, force)

    def set_animation_fps(self, fps):
        self._animation_delay = 1000 / fps

    def set_destination(self, x_position, y_position, speed):
        self.speed = speed
        self.destination = Vector2(x_position, y_position)
        self.direction = (self.destination - Vector2(self.pos)).normalize()

    def set_direction(self, vector):
        self.direction = vector

    def set_flip_x(self, flip):
        self.flip_x = flip

    def set_flip_y(self, flip):
        self.flip_y = flip

    def set_invincible(self, offset):
        self._invincible_initial_time = pygame.time.get_ticks() + offset
        self.invincible = True

    def set_position(self, x_position, y_position):
        self.pos = Vector2(x_position, y_position)

    def set_rotation(self, degrees):
        self.rotation = degrees

    def set_scale(self, scale):
        self.scale = scale

    def show(self):
        self._visible = True
        if self not in self.stage.actors:
            self.stage.actors.append(self)

        # FIXME: Should be underneath the if self not, but doesnot work
        if self.sound_spawn:
            self.sound_spawn.play()

    def strobe_start(self):
        self._strobe = True

    def strobe_stop(self):
        self._strobe = False
        self._visible = True
