from .base import Actor


class ActorBook01(Actor):
    animation_normal_files = ['players/W_Book01.png']
    animation_normal_dimensions = [(34, 34)]


class ActorBook02(Actor):
    animation_normal_files = ['players/W_Book02.png']
    animation_normal_dimensions = [(34, 34)]


class ActorBook03(Actor):
    animation_normal_files = ['players/W_Book04.png']
    animation_normal_dimensions = [(34, 34)]


class ActorBook04(Actor):
    animation_normal_files = ['players/W_Book05.png']
    animation_normal_dimensions = [(34, 34)]


class ActorBook05(Actor):
    animation_normal_files = ['players/W_Book06.png']
    animation_normal_dimensions = [(34, 34)]
